
<?php
class MyCalculator {
    protected $_num1, $_num2;


    public function __construct( $_num1, $_num2 ) {
        $this->_num1 = $_num1;
        $this->_num2 = $_num2;
    }

    public function add() {
        return $this->_num1 + $this->_num2;
    }


    public function subtract() {
        return $this->_num1 - $this->_num2;
    }

    public function multiply() {
        return $this->_num1 * $this->_num2;
    }


}

class B extends MyCalculator{

    public function divide() {
        return $this->_num1 / $this->_num2;
    }

}
$b = new B(20,0);
echo "<br>";
echo $b->add();
echo "<br>";
echo $b->multiply();
echo "<br>";
echo $b->subtract();
echo "<br>";
echo $b->divide();
?>


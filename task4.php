<?php
class factorial_of_a_number
{
    protected $_number;
    public function __construct($number)
    {
        if (!is_int($number))
        {
            throw new InvalidArgumentException('Not a number or missing argument');
        }
        $this->_number = $number;
    }

    public function result(){
        $_number = $this->_number;
        $factorial=1;
        for ($x=1; $x<=$_number; $x++)
        {
            $factorial = $factorial * $x;
        }
        return $factorial;
    }
}
$newfactorial = New factorial_of_a_number(7);

 echo $newfactorial->result();

?>
